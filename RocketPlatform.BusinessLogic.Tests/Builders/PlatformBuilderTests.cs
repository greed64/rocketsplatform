﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RocketsPlatform.BusinessLogic.Builders;
using RocketsPlatform.BusinessLogic.Commons;
using RocketsPlatform.BusinessLogic.Models;

namespace RocketPlatform.BusinessLogic.Tests.Builders
{
    [TestClass]
    public class PlatformBuilderTests
    {
        private IPlatformBuilder _platformBuilder;
        private SettingsProvider _settingsProviderFake;

        [TestInitialize]
        public void Setup()
        {
            _settingsProviderFake = Data.FakeData.GetFakeSettingsProvider();
            _platformBuilder = new PlatformBuilder(_settingsProviderFake);
        }

        [TestMethod]
        public void Buider_ShouldUseSettingsAndReturn_CorrectLandingAreaSize()
        {
            //Arrange
            var expectedLandingAreaHeight = 5;
            var expectedLandingAreaWidth = 6;

            //Act
            var result = _platformBuilder.Build();

            //Assert
            Assert.AreEqual(expectedLandingAreaHeight, result.PlatformArea.GetLength(0));
            Assert.AreEqual(expectedLandingAreaWidth, result.PlatformArea.GetLength(1));
        }

        [TestMethod]
        [DataRow(0, 0)]
        [DataRow(0, 5)]
        [DataRow(1, 1)]
        [DataRow(2, 1)]
        [DataRow(3, 1)]
        [DataRow(1, 5)]
        [DataRow(2, 5)]
        [DataRow(3, 1)]
        [DataRow(3, 2)]
        [DataRow(3, 5)]
        [DataRow(4, 1)]
        [DataRow(4, 2)]
        [DataRow(4, 5)]
        public void Buider_ShouldUseSettingsAndReturn_CorrectLandingAreaPosition(int x, int y)
        {
            //Arrange
            var expectedPlatformValue = PlatformFieldCodeEnum.FreeArea;

            //Act
            var result = _platformBuilder.Build().PlatformArea[x, y];

            //Assert
            Assert.AreEqual(expectedPlatformValue, (PlatformFieldCodeEnum)result);
        }

        [TestMethod]
        [DataRow(1, 2)]
        [DataRow(1, 3)]
        [DataRow(1, 4)]
        [DataRow(2, 2)]
        [DataRow(2, 3)]
        [DataRow(2, 4)]
        public void Buider_ShouldUseSettingsAndReturn_CorrectPlatformPosition(int x, int y)
        {
            //Arrange
            var expectedPlatformValue = PlatformFieldCodeEnum.FreePlatform;

            //Act
            var result = _platformBuilder.Build().PlatformArea[x, y];

            //Assert
            Assert.AreEqual(expectedPlatformValue, (PlatformFieldCodeEnum)result);
        }
    }
}