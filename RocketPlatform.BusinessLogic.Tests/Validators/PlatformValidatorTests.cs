﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RocketsPlatform.BusinessLogic.Models;
using RocketsPlatform.BusinessLogic.Validators;

namespace RocketPlatform.BusinessLogic.Tests.Validators
{
    [TestClass]
    public class PlatformValidatorTests
    {
        private IPlatformValidator _platformValidator;
        private Mock<IPlatformService> _platformServiceMock;

        [TestInitialize]
        public void Setup()
        {
            _platformServiceMock = new Mock<IPlatformService>();
            _platformValidator = new PlatformValidator(_platformServiceMock.Object);
        }

        [TestMethod]
        [DataRow(5, 5)]
        public void WhenRocketCanLandOnPlatform_ValidatorShouldReturn_OkResponse(int x, int y)
        {

            //Arrange
            _platformServiceMock.Setup(x => x.GetPlatformPoint(It.IsAny<int>(), It.IsAny<int>())).Returns(1);

            //Act
            var result = _platformValidator.Validate(x, y);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Out, result);
        }

        [TestMethod]
        [DataRow(5, 5)]
        public void WhenRocketTryLandOnFreeArea_ValidatorShouldReturn_OutResponse(int x, int y)
        {

            //Arrange
            _platformServiceMock.Setup(x => x.GetPlatformPoint(It.IsAny<int>(), It.IsAny<int>())).Returns(1);

            //Act
            var result = _platformValidator.Validate(x, y);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Out, result);
        }

        [TestMethod]
        [DataRow(5, 5)]
        public void WhenRocketCanLandOnTakenPlatform_ValidatorShouldReturn_ClashResponse(int x, int y)
        {

            //Arrange
            _platformServiceMock.Setup(x => x.GetPlatformPoint(It.IsAny<int>(), It.IsAny<int>())).Returns(-2);

            //Act
            var result = _platformValidator.Validate(x, y);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Clash, result);
        }

        [TestMethod]
        [DataRow(5, 5)]
        public void WhenRocketCanLandOnTakenArea_ValidatorShouldReturn_ClashResponse(int x, int y)
        {

            //Arrange
            _platformServiceMock.Setup(x => x.GetPlatformPoint(It.IsAny<int>(), It.IsAny<int>())).Returns(-1);

            //Act
            var result = _platformValidator.Validate(x, y);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Clash, result);
        }

        [TestMethod]
        [DataRow(5, 5)]
        public void WhenRocketCanLandUsingWrongCoordinats_ValidatorShouldReturn_ErrorResponse(int x, int y)
        {

            //Act
            var result = _platformValidator.Validate(x, y);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Error, result);
        }
    }
}