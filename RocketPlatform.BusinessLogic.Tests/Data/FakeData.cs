﻿using RocketsPlatform.BusinessLogic.Commons;
using RocketsPlatform.BusinessLogic.Models;

namespace RocketPlatform.BusinessLogic.Tests.Data
{
    internal static class FakeData
    {
        internal static SettingsProvider GetFakeSettingsProvider()
        {
            return new SettingsProvider
            {
                AppSettings = new AppSettings
                {
                    LandingArea = new LandingArea
                    {
                        Height = 5,
                        Width = 6
                    },
                    LandingPlatform = new LandingPlatform
                    {
                        Height = 2,
                        Width = 3
                    },
                    LandingPlatformPosition = new LandingPlatformPosition
                    {
                        StartHeight = 1,
                        StartWidth = 2
                    }
                }
            };
        }

        internal static Rocket GetFakeRocket()
        {
            return new Rocket(1, 1);
        }
    }
}