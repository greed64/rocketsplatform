﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RocketsPlatform.BusinessLogic.Models;
using RocketsPlatform.BusinessLogic.Services;
using RocketsPlatform.BusinessLogic.Validators;
using System.Threading.Tasks;

namespace RocketPlatform.BusinessLogic.Tests.Services
{
    [TestClass]
    public class LandingServiceTests
    {
        private ILandingService _landingService;
        private Mock<IPlatformService> _platformServiceMock;
        private Mock<IPlatformValidator> _platformValidatorMock;
        private Rocket _rocketFake;

        [TestInitialize]
        public void Setup()
        {
            _platformServiceMock = new Mock<IPlatformService>();
            _platformValidatorMock = new Mock<IPlatformValidator>();
            _rocketFake = Data.FakeData.GetFakeRocket();

            _landingService = new LandingService(_platformValidatorMock.Object, _platformServiceMock.Object);
        }

        [TestMethod]
        public async Task TryLand_ShouldReturn_ErrorResponse()
        {
            //Arrange
            _platformValidatorMock.Setup(x => x.Validate(It.IsAny<int>(), It.IsAny<int>())).Returns(LandingResponseEnum.Error);

            //Act
            var result = await _landingService.TryLand(_rocketFake);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Error, result);
            _platformServiceMock.Verify(x => x.TakePlace(_rocketFake), Times.Never);
        }

        [TestMethod]
        public async Task TryLand_ShouldReturn_OutResponse()
        {
            //Arrange
            _platformValidatorMock.Setup(x => x.Validate(It.IsAny<int>(), It.IsAny<int>())).Returns(LandingResponseEnum.Out);

            //Act
            var result = await _landingService.TryLand(_rocketFake);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Out, result);
            _platformServiceMock.Verify(x => x.TakePlace(_rocketFake), Times.Never);
        }

        [TestMethod]
        public async Task TryLand_ShouldReturn_ClashResponse()
        {
            //Arrange
            _platformValidatorMock.Setup(x => x.Validate(It.IsAny<int>(), It.IsAny<int>())).Returns(LandingResponseEnum.Clash);

            //Act
            var result = await _landingService.TryLand(_rocketFake);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Clash, result);
            _platformServiceMock.Verify(x => x.TakePlace(_rocketFake), Times.Never);
        }

        [TestMethod]
        public async Task TryLand_ShouldReturn_OkResponseAndLand()
        {
            //Arrange
            _platformValidatorMock.Setup(x => x.Validate(It.IsAny<int>(), It.IsAny<int>())).Returns(LandingResponseEnum.Ok);

            //Act
            var result = await _landingService.TryLand(_rocketFake);

            //Assert
            Assert.AreEqual(LandingResponseEnum.Ok, result);
            _platformServiceMock.Verify(x => x.TakePlace(_rocketFake), Times.Once);
        }
    }
}