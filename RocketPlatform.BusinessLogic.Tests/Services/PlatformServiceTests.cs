﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RocketsPlatform.BusinessLogic.Builders;
using RocketsPlatform.BusinessLogic.Commons;
using RocketsPlatform.BusinessLogic.Models;

namespace RocketPlatform.BusinessLogic.Tests.Services
{
    [TestClass]
    public class PlatformServiceTests
    {
        private IPlatformService _platformService;
        private IPlatformBuilder _platformBuilder;
        private SettingsProvider _settingsProviderFake;

        [TestInitialize]
        public void Setup()
        {
            _settingsProviderFake = Data.FakeData.GetFakeSettingsProvider();
            _platformBuilder = new PlatformBuilder(_settingsProviderFake);

            _platformService = new PlatformService(_settingsProviderFake, _platformBuilder);
        }

        [TestMethod]
        [DataRow(0, 0, PlatformFieldCodeEnum.FreeArea)]
        [DataRow(0, 5, PlatformFieldCodeEnum.FreeArea)]
        [DataRow(1, 1, PlatformFieldCodeEnum.FreeArea)]
        [DataRow(2, 1, PlatformFieldCodeEnum.FreeArea)]
        [DataRow(1, 2, PlatformFieldCodeEnum.FreePlatform)]
        [DataRow(1, 3, PlatformFieldCodeEnum.FreePlatform)]
        [DataRow(1, 4, PlatformFieldCodeEnum.FreePlatform)]
        public void GetPlatformPoint_ShouldReturn_CorrectPoint(int x, int y, PlatformFieldCodeEnum expectedValue)
        {
            //Act
            var result = _platformService.GetPlatformPoint(x, y);

            //Assert
            Assert.AreEqual(expectedValue, (PlatformFieldCodeEnum)result);
        }

        [TestMethod]
        [DataRow(1, 1, PlatformFieldCodeEnum.TakenArea)]
        [DataRow(1, 2, PlatformFieldCodeEnum.TakenPlatform)]
        [DataRow(1, 3, PlatformFieldCodeEnum.TakenPlatform)]
        [DataRow(2, 1, PlatformFieldCodeEnum.TakenArea)]
        [DataRow(2, 2, PlatformFieldCodeEnum.TakenPlatform)]
        [DataRow(2, 3, PlatformFieldCodeEnum.TakenPlatform)]
        [DataRow(3, 1, PlatformFieldCodeEnum.TakenArea)]
        [DataRow(3, 2, PlatformFieldCodeEnum.TakenArea)]
        [DataRow(3, 3, PlatformFieldCodeEnum.TakenArea)]
        public void TakePlace_ShoudSet_TakenPlatform(int x, int y, PlatformFieldCodeEnum expectedPlatformFieldValue)
        {
            //Arrange
            var rocket = new Rocket(2, 2);

            //Act
            _platformService.TakePlace(rocket);
            var result = _platformService.GetPlatformPoint(x, y);

            //Assert
            Assert.AreEqual(expectedPlatformFieldValue, (PlatformFieldCodeEnum)result);
        }
    }
}
