# README #

Hello, Please read me!

### About solution ###

* Fulfilled requirements
* "Production-close" code structure
* Design patterns & clean code (in most cases ;))
* Wide tests
* Created little complex version: 
* - has area and platform configuration
* - remember each rocket, not only last one

### What could I do better ###

* Refactor TakePlace method in PlatformService class
* Create coordinates class/ structure instead of using x,y
* Finish LeavePlace method in PlatformService class
* Add logging in error case, e.g. GetPlatformPoint method in PlatformValidator
* Many small but very time consuming details ;)
