﻿using System.ComponentModel;

namespace RocketsPlatform.BusinessLogic.Models
{
    public enum LandingResponseEnum
    {
        [Description("ok for landing")]
        Ok,
        [Description("out of platform")]
        Out,
        [Description("something gets wrong")]
        Error,
        [Description("clash")]
        Clash
    }
}