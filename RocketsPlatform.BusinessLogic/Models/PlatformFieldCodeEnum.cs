﻿using System.ComponentModel;

namespace RocketsPlatform.BusinessLogic.Models
{
    public enum PlatformFieldCodeEnum
    {
        [Description("safe area")]
        FreeArea = 1,
        [Description("platform place")]
        FreePlatform = 2,
        [Description("safe area taken by rocket")]
        TakenArea = -1,
        [Description("platform place taken by rocket")]
        TakenPlatform = -2,
    }
}