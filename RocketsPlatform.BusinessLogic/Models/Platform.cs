﻿namespace RocketsPlatform.BusinessLogic.Models
{
    public class Platform
    {
        public short[,] PlatformArea { get; internal set; }
    }
}
