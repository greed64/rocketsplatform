﻿namespace RocketsPlatform.BusinessLogic.Models
{
    public class Rocket
    {
        public int LandingPointX { get; private set; }
        public int LandingPointY { get; private set; }
        public int RocketTakenPlaceX { get; private set; }
        public int RocketTakenPlaceY { get; private set; }
        public int RocketSize { get; private set; }

        public Rocket(int landingPlaceX, int landingPlaceY)
        {
            LandingPointX = landingPlaceX;
            LandingPointY = landingPlaceY;

            //TODO: different rocket size - out of scope
            RocketSize = 1;
            RocketTakenPlaceX = 3;
            RocketTakenPlaceY = 3;
        }
    }
}