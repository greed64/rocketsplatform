﻿namespace RocketsPlatform.BusinessLogic.Commons
{
    public sealed class SettingsProvider
    {
        public AppSettings AppSettings { get; set; }
    }

    public sealed class AppSettings
    {
        public LandingArea LandingArea { get; set; }
        public LandingPlatform LandingPlatform { get; set; }
        public LandingPlatformPosition LandingPlatformPosition { get; set; }
    }

    public sealed class LandingArea
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public sealed class LandingPlatform
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public sealed class LandingPlatformPosition
    {
        public int StartWidth { get; set; }
        public int StartHeight { get; set; }
    }
}
