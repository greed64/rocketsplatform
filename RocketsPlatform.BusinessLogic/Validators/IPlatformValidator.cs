﻿using RocketsPlatform.BusinessLogic.Models;

namespace RocketsPlatform.BusinessLogic.Validators
{
    public interface IPlatformValidator
    {
        LandingResponseEnum Validate(int x, int y);
    }
}