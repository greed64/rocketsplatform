﻿using RocketsPlatform.BusinessLogic.Models;
using System;

namespace RocketsPlatform.BusinessLogic.Validators
{
    public class PlatformValidator : IPlatformValidator
    {
        private readonly IPlatformService _platformService;

        public PlatformValidator(IPlatformService platformService)
        {
            _platformService = platformService;
        }

        public LandingResponseEnum Validate(int x, int y)
        {
            var platformPoint = GetPlatformPoint(x, y);

            switch ((PlatformFieldCodeEnum)platformPoint)
            {
                case PlatformFieldCodeEnum.FreePlatform:
                    return LandingResponseEnum.Ok;
                case PlatformFieldCodeEnum.FreeArea:
                    return LandingResponseEnum.Out;
                case PlatformFieldCodeEnum.TakenArea:
                    return LandingResponseEnum.Clash;
                case PlatformFieldCodeEnum.TakenPlatform:
                    return LandingResponseEnum.Clash;
                default:
                    return LandingResponseEnum.Error;
            }
        }

        private short GetPlatformPoint(int x, int y)
        {
            short platformPoint = 0;

            try
            {
                platformPoint = _platformService.GetPlatformPoint(x, y);
            }
            catch (IndexOutOfRangeException ex)
            {
                //TODO: error loging - out of scope
            }
            catch (Exception ex)
            {
                //TODO: error loging - out of scope
            }

            return platformPoint;
        }
    }
}