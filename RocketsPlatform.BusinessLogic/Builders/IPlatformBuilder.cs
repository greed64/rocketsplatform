﻿using RocketsPlatform.BusinessLogic.Models;

namespace RocketsPlatform.BusinessLogic.Builders
{
    public interface IPlatformBuilder
    {
        Platform Build();
    }
}
