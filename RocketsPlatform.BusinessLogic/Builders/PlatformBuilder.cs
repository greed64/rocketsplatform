﻿using RocketsPlatform.BusinessLogic.Commons;
using RocketsPlatform.BusinessLogic.Models;

namespace RocketsPlatform.BusinessLogic.Builders
{
    public class PlatformBuilder : IPlatformBuilder
    {
        private readonly SettingsProvider _settingsProvider;
        private Platform platform;

        public PlatformBuilder(SettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public Platform Build()
        {
            platform = new Platform();
            BuildSafeArea(_settingsProvider.AppSettings.LandingArea);
            BuildPlatformArea(_settingsProvider.AppSettings.LandingPlatform, _settingsProvider.AppSettings.LandingPlatformPosition);

            return platform;
        }

        private void BuildSafeArea(LandingArea landingArea)
        {
            platform.PlatformArea = new short[landingArea.Height, landingArea.Width];

            for (int i = 0; i < landingArea.Width; i++)
            {
                for (int j = 0; j < landingArea.Height; j++)
                {
                    platform.PlatformArea[j, i] = (short)PlatformFieldCodeEnum.FreeArea;
                }
            }
        }

        private void BuildPlatformArea(LandingPlatform landingPlatform, LandingPlatformPosition landingPlatformPlace)
        {
            for (int i = landingPlatformPlace.StartWidth; i < landingPlatform.Width + landingPlatformPlace.StartWidth; i++)
            {
                for (int j = landingPlatformPlace.StartHeight; j < landingPlatform.Height + landingPlatformPlace.StartHeight; j++)
                {
                    platform.PlatformArea[j, i] = (short)PlatformFieldCodeEnum.FreePlatform;
                }
            }
        }
    }
}
