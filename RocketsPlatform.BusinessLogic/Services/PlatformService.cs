﻿using RocketsPlatform.BusinessLogic.Builders;
using RocketsPlatform.BusinessLogic.Commons;

namespace RocketsPlatform.BusinessLogic.Models
{
    public class PlatformService : IPlatformService
    {
        private readonly SettingsProvider _settingsProvider;
        private readonly IPlatformBuilder _platformBuilder;

        private static readonly object padlock = new object();

        private Platform _platform;

        public PlatformService(SettingsProvider settingsProvider, IPlatformBuilder platformBuilder)
        {
            _settingsProvider = settingsProvider;
            _platformBuilder = platformBuilder;
            InitPlatform();
        }

        public short GetPlatformPoint(int x, int y)
        {
            return _platform.PlatformArea[x, y];
        }

        public void TakePlace(Rocket rocket)
        {
            var startPositionX = rocket.LandingPointX - ((rocket.RocketTakenPlaceX - rocket.RocketSize) / 2);
            var startPositionY = rocket.LandingPointY - ((rocket.RocketTakenPlaceY - rocket.RocketSize) / 2);

            var takenPlaceX = rocket.RocketTakenPlaceX;
            var takenPlaceY = rocket.RocketTakenPlaceY;

            if (startPositionX + rocket.RocketTakenPlaceX > _settingsProvider.AppSettings.LandingArea.Width)
            {
                takenPlaceX = _settingsProvider.AppSettings.LandingArea.Width - startPositionX;
            }

            if (startPositionY + rocket.RocketTakenPlaceY > _settingsProvider.AppSettings.LandingArea.Height)
            {
                takenPlaceY = _settingsProvider.AppSettings.LandingArea.Width - startPositionY;
            }

            for (int i = startPositionX; i < startPositionX + takenPlaceX; i++)
            {
                for (int j = startPositionY; j < startPositionX + takenPlaceY; j++)
                {
                    _platform.PlatformArea[j, i] = (short)(-System.Math.Abs(_platform.PlatformArea[j, i]));
                }
            }
        }

        private void InitPlatform()
        {
            lock (padlock)
            {
                if (_platform == null)
                {
                    _platform = _platformBuilder.Build();
                }
            }
        }
    }
}