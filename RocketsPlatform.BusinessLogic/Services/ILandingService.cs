﻿using RocketsPlatform.BusinessLogic.Models;
using System.Threading.Tasks;

namespace RocketsPlatform.BusinessLogic.Services
{
    public interface ILandingService
    {
        Task<LandingResponseEnum> TryLand(Rocket rocket);

        Task LeavePlace(Rocket rocket);
    }
}