﻿namespace RocketsPlatform.BusinessLogic.Models
{
    public interface IPlatformService
    {
        short GetPlatformPoint(int x, int y);

        void TakePlace(Rocket rocket);
    }
}