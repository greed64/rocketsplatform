﻿using RocketsPlatform.BusinessLogic.Models;
using RocketsPlatform.BusinessLogic.Validators;
using System;
using System.Threading.Tasks;

namespace RocketsPlatform.BusinessLogic.Services
{
    public class LandingService : ILandingService
    {
        private readonly IPlatformValidator _platformValidator;
        private readonly IPlatformService _platformService;

        public LandingService(IPlatformValidator platformValidator, IPlatformService platformService)
        {
            _platformValidator = platformValidator;
            _platformService = platformService;
        }

        public async Task<LandingResponseEnum> TryLand(Rocket rocket)
        {
            var validate = _platformValidator.Validate(rocket.LandingPointX, rocket.LandingPointY);

            if (validate == LandingResponseEnum.Ok) 
            {
                await Land(rocket);
            }

            return validate;
        }

        public async Task LeavePlace(Rocket rocket)
        {
            //TODO: leave pleace - out of scope
            throw new NotImplementedException();
        }

        private async Task Land(Rocket rocket)
        {
            await Task.Run(() => _platformService.TakePlace(rocket));
        }
    }
}