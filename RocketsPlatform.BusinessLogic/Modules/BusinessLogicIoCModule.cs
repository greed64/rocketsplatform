﻿using Autofac;
using System.Reflection;

namespace RocketsPlatform.BusinessLogic.Modules
{
    public class BusinessLogicIoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}