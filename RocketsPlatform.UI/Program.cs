﻿using Autofac;
using Microsoft.Extensions.Configuration;
using RocketsPlatform.BusinessLogic.Commons;
using RocketsPlatform.UI.Modules;
using System.Threading.Tasks;

namespace RocketsPlatform.UI
{
    class Program
    {
        static async Task Main()
        {
            var containerBuilder = new ContainerBuilder();

            var configuration = new ConfigurationBuilder()
             .AddJsonFile("appsettings.json")
             .Build();

            containerBuilder.RegisterInstance(configuration.Get<SettingsProvider>()).SingleInstance();
            containerBuilder.RegisterModule<UIIoCModule>();

            var container = containerBuilder.Build();

            var startup = container.Resolve<Startup>();
            await startup.Start();
        }
    }
}