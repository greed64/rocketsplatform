﻿using Autofac;
using RocketsPlatform.BusinessLogic.Modules;
using System.Reflection;

namespace RocketsPlatform.UI.Modules
{
    class UIIoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<Program>().AsSelf();
            builder.RegisterType<Startup>().AsSelf();
            builder.RegisterModule<BusinessLogicIoCModule>();

            base.Load(builder);
        }
    }
}