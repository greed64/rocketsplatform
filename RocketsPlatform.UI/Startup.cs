﻿using RocketsPlatform.BusinessLogic.Models;
using RocketsPlatform.BusinessLogic.Services;
using RocketsPlatform.BusinessLogic.Utils;
using System;
using System.Threading.Tasks;

namespace RocketsPlatform.UI
{
    public class Startup
    {
        private readonly ILandingService _landingService;

        public Startup(ILandingService landingService)
        {
            _landingService = landingService;
        }
        public async Task Start()
        {
            var rocketApollo11 = new Rocket(5, 5);
            var canLandApollo11 = await _landingService.TryLand(rocketApollo11);

            var rocketApollo13 = new Rocket(6, 6);
            var canLandApollo13 = await _landingService.TryLand(rocketApollo13);


            Console.WriteLine("Apollo11: " + AttributeUtil.GetDescription(canLandApollo11));
            Console.WriteLine("Apollo13 " + AttributeUtil.GetDescription(canLandApollo13));

            Console.ReadKey();
        }
    }
}